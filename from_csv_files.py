from data import *
from special_behavior import *
import json, time

#dimcombinations([999, 999], [sexe, reg], 0)

results = specialbehavior(ATCL, [SEXEL, REGL], ['SEXE', 'REG'], 'AGE', 5, True)
# results = specialbehavior(ATCL, [AGEL, REGL], ['AGE', 'REG'], 'SEXE', 5, True)
filename = './results'+ str(time.time()) +'.json'
print('Write results to file ...')
with open(filename, 'w') as resfile :
    json.dump(results, resfile)
