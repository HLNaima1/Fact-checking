# Enumerates all the combinations of dimensions' values
# Takes a list of sets of dimensions' values
# Returns a tree discribing the obtained hierarchy
from hashlib import md5
from treelib import Tree, Node
import cx_Oracle
from databaserequests import *

def treebuild(root_node_content, dimensions, root_val) :
    tree = Tree()
    nodesidentifier = md5(str(root_node_content).encode('utf-8')).hexdigest()
    tree.create_node(root_node_content, nodesidentifier)
    tree = enumerate(tree, root_node_content, nodesidentifier, dimensions, root_val)
    return tree

def enumerate(tree, node_content, node_id, dimensions, root_val) :
     # Find the next element to replace
     nbre_dimensions = len(dimensions)

     next_elt, i, found = 0, nbre_dimensions - 1, False

     while i >= 0 and not found :
         if node_content[i] != root_val :
             found = True
             if i == nbre_dimensions - 1 :
                 return tree
             next_elt = i + 1
         else :
             i -= 1

     # Create the node's children
     for i in range(next_elt, nbre_dimensions) :
         # Change the value of the position i only
         for value in dimensions[i] :
             new_node = node_content[:]
             new_node[i] = value
             nodesidentifier = md5(str(new_node).encode('utf-8')).hexdigest()
             tree.create_node(new_node, nodesidentifier, parent=node_id)
             # Build recursively the children of the new node
             tree = enumerate(tree, new_node, nodesidentifier, dimensions, root_val)


     return tree

def build_ATC_hierarchy() :

    tree = Tree()
    tree.create_node("All", "all")

    atc5_list = get_ATC5()

    for atc5 in atc5_list:

        if tree.get_node(atc5[0:1].lower()) is None:
            tree.create_node(atc5[0:1],atc5[0:1].lower(), parent="all")

        if tree.get_node(atc5[0:3].lower()) is None:
            tree.create_node(atc5[0:3], atc5[0:3].lower(), parent=atc5[0:1].lower())

        if tree.get_node(atc5[0:4].lower()) is None:
            tree.create_node(atc5[0:4], atc5[0:4].lower(), parent=atc5[0:3].lower())

        if tree.get_node(atc5[0:5].lower()) is None:
            tree.create_node(atc5[0:5], atc5[0:5].lower(), parent=atc5[0:4].lower())

        if tree.get_node(atc5[0:7].lower()) is None:
            tree.create_node(atc5[0:7], atc5[0:7].lower(), parent=atc5[0:5].lower())
    return tree
