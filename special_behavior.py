from data import *
from distance import *

# The atclists must contain a list for each level of atcs to process
# for each list we should keep an index
def specialbehavior(atclists, dimlists, dim_names_list, dim_name, limit, knownonly) :

    dataset = joindata(knownonly)
    indexes = [0 for x in atclists]
    results = {0 : 0}
    # Treatements for all atcs
    print('ALL')

    atc1 = atclists[0]
    for i in range(0, len(atc1)) :
        results = dimcombinations([999, 999], dimlists, dim_names_list, 0, {'atcname' : 'ATC1', 'val' : atc1[i]}, results, dataset, dim_name, limit)
        indexes, results = atcchildren(atc1[i], atclists, indexes, results, 1, dimlists, dim_names_list, dataset, dim_name, limit)

    return results

def atcchildren(parent, atclists, indexes, results, level, dimlists, dim_names_list, dataset, dim_name, limit) :
    if level == len(atclists) :
        return indexes, results
    else :
        i = indexes[level]
        atc = atclists[level]
        parentlen = len(parent)
        while i < len(atc) and atc[i][0:parentlen] == parent :
            if len(atc[i]) > parentlen :
                results = dimcombinations([999, 999], dimlists, dim_names_list, 0, {'atcname' : 'ATC'+str(level+1), 'val' : atc[i]}, results, dataset, dim_name, limit)
                indexes, results = atcchildren(atc[i], atclists, indexes, results, level + 1, dimlists, dim_names_list, dataset, dim_name, limit)
            i += 1
        indexes[level] = i
        return indexes, results

def dimcombinations(parent, dimlists, dim_names_list, level, atc, results, dataset, dim_name, limit) :
    n = len(dimlists)
    if level == n :
        return results
    for i in range(level, n) :
        values = dimlists[i]
        for val in values :
            child = [p for p in parent]
            child[i] = val

            # compute distance to all
            print('(level, val, atc) = (', level, ' , ',child, ' , ', atc['val'], ')')
            context = {}
            for d in range(0, n) :
                if child[d] != 999 :
                    context[dim_names_list[d]] = child[d]
            # All medicines
            boxes_all_medocs = getboxes(dim_name, context, dataset)


            if atc['val'] != 'All' :
                context[atc['atcname']] = atc['val']

            # For a specific atc
            boxes_c_medocs = getboxes(dim_name, context, dataset)

            # print('Distance between : ', boxes_c_medocs, ' and ', boxes_all_medocs)
            dist = distance(normalize(boxes_all_medocs), normalize(boxes_c_medocs))
            # print('=> ', dist)
            distmin = min(results.keys())

            if len(results) < limit :
                if dist in results :
                    results[dist].append({'context' : context, 'spec' : boxes_c_medocs, 'all' : boxes_all_medocs })
                else :
                    results[dist] = [{'context' : context, 'spec' : boxes_c_medocs, 'all' : boxes_all_medocs }]
                print('======= Results list updated ========> ', results.keys())

            elif dist > distmin :
                print(dist, ' > ', distmin)
                if dist in results :
                    results[dist].append({'context' : context, 'spec' : boxes_c_medocs, 'all' : boxes_all_medocs })
                else :
                    del results[distmin]
                    results[dist] = [{'context' : context, 'spec' : boxes_c_medocs, 'all' : boxes_all_medocs }]
                print('======= Results list updated ========> ', results.keys())

            results = dimcombinations(child, dimlists, dim_names_list, level + i + 1, atc, results, dataset, dim_name, limit)
    return results
