import pandas as pd
from datafromcsv import *

directory = '../openmedic/'

knownonly = True

SEXEL, REGL, AGEL = getsexeregage(directory, knownonly)
dimension_values = {'AGE' : AGEL, 'SEXE' : SEXEL, 'REG' : REGL}

ATCL = getATClists(directory)

def joindata (knownonly) :
    # Load tables
    medicines = pd.read_csv('../openmedic/medicines.csv')
    recipients = pd.read_csv('../openmedic/recipients.csv')
    spendings = pd.read_csv('../openmedic/spendings.csv')

    if knownonly :
        recipients = recipients[(recipients['SEXE'] != 9) & (recipients['AGE'] != 99) & (recipients['REG'] != 9) & (recipients['REG'] != 0)]
    # join tables
    spend = spendings.set_index('MEDICINE_ID').join(medicines.set_index('MEDICINE_ID')).set_index('RECIPIENT_ID').join(recipients.set_index('RECIPIENT_ID'))
    return spend

def constraint(context, df) :
    ok = True
    for key in context :
        ok = ok & (df[key] == context[key])
    return ok

def getboxes(dim_name, context, spendings) :

    # Apply the constraints
    spendings = spendings[lambda df : constraint(context, df)]

    # Group by the specified dimension and sum the number of boxes
    boxes = spendings.groupby([dim_name])['BOXES'].sum()

    # Format result
    dim_values = dimension_values[dim_name]
    res = {}

    keys = boxes.index

    for d in dim_values :
        if d in keys :
            res[d] = boxes.loc[d]
        else :
            res[d] = 0

    return res
