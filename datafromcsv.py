from pandas import read_csv, DataFrame


def getATClists(directory) :
    df = read_csv(directory + 'medicines.csv')
    atclists = []
    for i in range(0, 5) :
        atclists.append(sorted(set(df['ATC'+str(i+1)])))

    return atclists


def getsexeregage(directory, knownonly) :
    df = read_csv(directory + 'recipients.csv')

    df = df[(df['SEXE'] != 9) & (df['AGE'] != 99) & (df['REG'] != 9) & (df['REG'] != 0)]

    sexe = sorted(set(df['SEXE']))
    age = sorted(set(df['AGE']))
    reg = sorted(set(df['REG']))

    return (sexe, reg, age)
