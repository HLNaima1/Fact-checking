def normalize(v) :
    s = sum(v.values())
    if s == 0 :
        return v
    for x in v :
        v[x] /= s
    return v

def distance(v1, v2) :
    dist = 0
    for i in v1 :
        dist += abs(v1[i] - v2[i])
    return dist
