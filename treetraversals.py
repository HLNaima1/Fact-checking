from distance import *
from databaserequests import get_boxes_per_person_per_age

def dfs(tree, node, max_level) :
     print(node.tag)
     if max_level != 0 and not node.is_leaf() :
         for n in tree.children(node.identifier) :
             dfs(tree, n, max_level - 1)

def postorder_dfs(tree, node, depth, k_most_different) :
    if depth != 0 and node :
        if node.is_leaf() :
            postorder_dfs(tree, None, depth - 1, k_most_different)
        else :
            for n in tree.children(node.identifier) :
                k_most_different = postorder_dfs(tree, n, depth - 1, k_most_different)
        print('* ', node.tag)
        bfs(tree, 2, k_most_different)
    return k_most_different

def bfs(tree, depth, k_most_different) :
    root = tree.get_node(tree.root)
    if not root :
        return []

    nodes_not_yet_visited = []
    nodes_not_yet_visited.append(root)

    while len(nodes_not_yet_visited) > 0 :
        node = nodes_not_yet_visited.pop(0)
        print(node.tag)
        if not node.is_leaf() and tree.depth(node) < depth:
            for n in tree.children(node.identifier) :
                nodes_not_yet_visited.append(n)

    return k_most_different

def atc_traversal(tree, depth, indiv_node, k_most_different, k, attrs, dim_name, dim_val) :
    root = tree.get_node(tree.root)
    if not root :
        return []

    nodes_not_yet_visited = []
    nodes_not_yet_visited.append(root)
    min_ = 0

    while len(nodes_not_yet_visited) > 0 :
        node = nodes_not_yet_visited.pop(0)

        ###### Calculate distance #####
        print(node.tag, ':', indiv_node.tag )
        context_spec = {}
        i = 0
        for attr in attrs :
            context_spec[attr] = indiv_node.tag[i]
            i += 1

        vect_all_medicines = get_boxes_per_person_per_age(context_spec, dim_name, dim_val, 'All', 0)
        vect_c_medicines = get_boxes_per_person_per_age(context_spec, dim_name, dim_val, node.tag, tree.depth(node))

        print(vect_all_medicines, ' - ', vect_c_medicines)

        d = distance(vect_all_medicines, vect_c_medicines)

        c = 0
        for v in vect_c_medicines :
            c += vect_c_medicines[v]
        if c == 0 :
            print(node.tag, ' not consumed !')
        else:
            if len(k_most_different) < k :
                k_most_different.append(d)
            else :
                min_ = min(k_most_different)
                if d > min_ :
                    k_most_different.remove(min_)
                    k_most_different.append(d)
            print(d, 'Comapred to ', min_)
            print(k_most_different)
        ###### End Calculate distance #####

        if not node.is_leaf() and tree.depth(node) < depth:
            for n in tree.children(node.identifier) :
                nodes_not_yet_visited.append(n)

    return k_most_different

def most_different_contexts(tree, node, depth, atc_tree, atc_level, k_most_different, k, attrs, dim_name, dim_val) :
    if depth != 0 and node :
        if node.is_leaf() :
            most_different_contexts(tree, None, depth - 1, atc_tree, atc_level, k_most_different, k, attrs, dim_name, dim_val)
        else :
            for n in tree.children(node.identifier) :
                k_most_different = most_different_contexts(tree, n, depth - 1, atc_tree, atc_level, k_most_different, k, attrs, dim_name, dim_val)
        print(node.tag)
        # For this node traverse all the atc tree
        k_most_different = atc_traversal(atc_tree, atc_level, node, k_most_different, k, attrs, dim_name, dim_val)

    return k_most_different
