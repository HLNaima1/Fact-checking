from data import *

spendings = joindata()
dim_name = 'AGE'
context = {'REG' : 27, 'ATC2' : 'A01'}

results = getboxes(dim_name, context, spendings)

print('Here are the results : ')
print(results)
