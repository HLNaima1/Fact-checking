# Fact-checking project 

## Environment settlements 

Start by clonnig the project using :  
```bash
    $ git clone https://gitlab.com/HLNaima1/Fact-checking.git
```  
You get a directory named `Fact-checking`, get into that directory :  
```bash
    $ cd Fact-checking
```  
**NB :** You are on `develop`, think of creating a new branch to start working on a new feature :p :  
```bash
    $ git checkout -b feature/myfeaturename
```  
### Setup and/or start a python virtual environemnt
Since python packages exist in numerous versions, and to avoid dealing with different versions installed conflicts, we're going to use a python
[virtual environement](https://docs.python.org/3/tutorial/venv.html)  
#### Installation
Use the command below to install `python3-venv` :  
```bash
    $ sudo apt-get install python3-venv
```  
#### Create and/or activate a virtual environment
Create a virtual environment named `virtualenv` (ignored by the git, if you choose another name, please add it to the `.gitignore` file !)   
```bash
    $ python3 -m venv virtualenv
```  
**NB :** You only need to create your virtual environment once !  
Activate the virtual environment **each time you want to run the project** :
```bash
    $ source virtualenv/bin/activate
```  
To install the required python packages needed to run the project, or new added ones use the command below once your virtual environment is activated :  
```bash
    $ pip install -r requirements.txt
```  
#### Install new packages
To install a new package use `pip install pckg_name` (once your virtual environement is activated!)  
Don't forget to add the new package to `requirements.txt` using the command in the **project root** :    
```bash
    $ pip freeze > requirements.txt
```  
### Requirements
* Installing python3 and python3-venv
* Creating and activating a virtual environement
* Installing the required packages contained in `requirements.txt` in the virtual environement 

**Now you're good to go farther :)**

### cx_oracle requirements
Follow the instructions on this [page](https://oracle.github.io/odpi/doc/installation.html#linux) to install the `oracle instant client` needed to get cx_oracle connection running.  

## Data from CSV files
You'll need to create a directory named `../openmedic` and export the database files under the following names :  
* medicines.csv
* prescribers.csv
* recipients.csv
* spendings.csv

Start the script using the following command line :  

```bash
    $ python3 from_csv_files.py
``` 

## Data from the Oracle database
Start the script using the following command line :  

```bash
    $ python3  from_oracle_db.py 
``` 

## Results
The interpretation of the results is explained in the report.

