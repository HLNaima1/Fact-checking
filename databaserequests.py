import cx_Oracle

ip = 'if-oracle15'
port = 1521
SID = 'DB15'
user = 'DS_OPENMEDIC'
mdp = 'mdporacle'

def oracle_db_connect() :
    dsn_tns = cx_Oracle.makedsn(ip, port, SID)
    con = cx_Oracle.connect(user, mdp, dsn_tns)
    return con

def get_ATC5() :
    con = oracle_db_connect()
    cursor = con.cursor()
    cursor.execute("""
    SELECT distinct a5.ATC5 from ATC5 a5
    """)
    return [row[0] for row in cursor]

def get_regions() :
    con = oracle_db_connect()
    cursor = con.cursor()
    cursor.execute("""
    SELECT distinct REG from RECIPIENTS WHERE REG <> 0
    """)
    return [row[0] for row in cursor]


def get_sexe() :
    con = oracle_db_connect()
    cursor = con.cursor()
    cursor.execute("""
    SELECT distinct SEXE from RECIPIENTS where SEXE <> 9
    """)
    return [row[0] for row in cursor]


def get_age() :
    con = oracle_db_connect()
    cursor = con.cursor()
    cursor.execute("""
    SELECT distinct AGE from RECIPIENTS where AGE <> 99
    """)
    return [row[0] for row in cursor]


dim_fcts = {'sexe': get_sexe, 'reg': get_regions, 'age': get_age}

def get_boxes_per_person_per_age(context_attributes, dim_name, dim_val, atc, atc_level) :
    con = oracle_db_connect()
    cursor = con.cursor()

    context_spec = ''
    attributes = dim_name.upper()
    join = 'RECIPIENTS.{} = ALLREC.{} '.format(dim_name, dim_name)
    attributes_rec = 'RECIPIENTS.' + dim_name.upper()
    for attr in context_attributes :
        if context_attributes[attr] == 999 :
            context_spec += ''
        else :
            context_spec += ' AND ' + attr.upper() + ' = ' + str(context_attributes[attr])
            attributes += ', ' + attr.upper()
            attributes_rec += ', RECIPIENTS.' + attr.upper()
            join += 'AND RECIPIENTS.{} = ALLREC.{} '.format(attr.upper(), attr.upper())

    recipients = """
    WITH ALLREC AS(
    SELECT {}, sum(POPULATION) POPULATION
    FROM RECIPIENTS
    GROUP BY {})

    SELECT RECIPIENT_ID, {}, ALLREC.POPULATION
    FROM ALLREC
    INNER JOIN RECIPIENTS ON {}
    """.format(attributes, attributes, attributes_rec, join)

    if atc == 'All' :
        atc_spec = ' '
    else :
        atc_spec = "AND MEDICINE_ID IN (SELECT MEDICINE_ID from MEDICINES WHERE ATC{} = '{}') ".format(atc_level, atc)

    req = """
    SELECT {}, SUM (BOXES)/POPULATION AS BOX_PER_PERSON
    FROM SPENDINGS
    JOIN (
            {}
    ) REC
    ON SPENDINGS.RECIPIENT_ID = REC.RECIPIENT_ID
    WHERE
        {} <> {}
        {}
        {}
    GROUP BY {}, POPULATION
    ORDER BY {}
    """.format(dim_name, recipients, dim_name, dim_val, context_spec, atc_spec, dim_name, dim_name)
    #print(req)
    cursor.execute(req)

    # Build the vector of consumption per AGE
    dim_vals = dim_fcts[dim_name.lower()]()
    vec = {}
    for v in dim_vals :
        vec[v] = 0

    for row in cursor :
        vec[row[0]] = row[1]

    return vec
