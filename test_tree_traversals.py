from treetraversals import *
from treelib import Tree, Node

tree = Tree()
tree.create_node(1, 1)
tree.create_node(2, 2, 1)
tree.create_node(3, 3, 1)
tree.create_node(4, 4, 2)
tree.create_node(5, 5, 2)
tree.create_node(6, 6, 4)
tree.create_node(7, 7, 4)
tree.create_node(8, 8, 5)
tree.create_node(9, 9, 5)
k_most_different = postorder_dfs(tree, tree.get_node(tree.root), 3, [])
# k_most_different = bfs(tree, tree.get_node(tree.root), 4, [])

# k_most_different = bfs(tree, 1, [])

print(k_most_different)
