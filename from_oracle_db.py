from treesbuilding import *
from databaserequests import get_boxes_per_person_per_age, get_age;
from distance import *
from treetraversals import *

# dimensions = [{'a1', 'a2', 'a3'}, {'b1', 'b2'}, {'c1', 'c2', 'c3', 'c4'}]
regions = get_regions()
print('Regions')
print(regions)

sexe = get_sexe()
print('Sexe')
print(sexe)

print('Building region-sexe combinations tree')
dimensions = [regions, sexe]
root_node_content = [999, 999]
region_sexe_tree = treebuild(root_node_content, dimensions, 999)
# region_sexe_tree.show()

print('ATC hierarchy tree')
atc_tree = build_ATC_hierarchy()
# atc.show()

vect_all_medicines = get_boxes_per_person_per_age({'sexe' : 1, 'reg' : 999}, 'AGE', 99, 'All', 0)
vect_c_medicines = get_boxes_per_person_per_age({'sexe' : 1, 'reg' : 999}, 'AGE', 99, 'A', 1)
print(vect_all_medicines, ' - ', vect_c_medicines)
print(distance(vect_all_medicines, vect_c_medicines))

# k_most_different = postorder_dfs(region_sexe_tree, region_sexe_tree.get_node(region_sexe_tree.root), 3, [])
# k_most_different = bfs(atc_tree, 3, [])

k_most_different = most_different_contexts(region_sexe_tree, region_sexe_tree.get_node(region_sexe_tree.root), 3, atc_tree, 2, [], 10, ['reg', 'sexe'], 'AGE', 99)
print(k_most_different)
